package com.ul.acl.model.players;


import com.ul.acl.model.cards.Card;
import com.ul.acl.model.cards.frenchcards.FrenchCard;

import java.util.Vector;

public class BelotePlayer extends Player {

    private Vector<Card> cards = new Vector<Card>();

    public BelotePlayer(String name) {
        super(name);
    }

    @Override
    public void addCard(Card frenchCard) {
        this.cards.add(frenchCard);
        this.updateScore((FrenchCard)frenchCard);
    }


    public void updateScore(FrenchCard frenchCard){
        this.score=this.score - frenchCard.getValue();
    }
}
