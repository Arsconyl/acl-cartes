package com.ul.acl.model.cards.frenchcards;

public  enum Suits {
    SPADES,
    HEARTS,
    DIAMONDS,
    CLUBS;
}