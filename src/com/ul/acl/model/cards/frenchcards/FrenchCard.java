package com.ul.acl.model.cards.frenchcards;


import com.ul.acl.model.cards.Card;

public class FrenchCard implements Card {

    private Suits symbol;
    private Rank rank;
    private int value;


    public FrenchCard(Suits symbol,Rank rank,int value) {

        this.symbol=symbol;
        this.rank=rank;
        this.value=value;
    }

    @Override
    public int getValue() {
        return this.value;
    }

    public Suits getSymbol() {
        return symbol;
    }

    public void setSymbol(Suits symbol) {
        this.symbol = symbol;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }


    @Override
    public String toString() {
        return "frenchcards{" +
                "symbol=" + symbol +
                ", rank=" + rank +
                ", value=" + value +
                '}';
    }
}
