package com.ul.acl.model.game;

import com.ul.acl.model.cards.Card;

import com.ul.acl.model.players.Player;

import java.util.Random;
import java.util.Vector;

public abstract class CardGame extends Game {

    protected Vector<Card> deck;

    public CardGame(String name, Player player) {
        super(name,player);
    }

    public Vector<Card> getDeck() {
        return deck;
    }


    @Override
    public String toString() {
        return "CardGame{" +
                "deck=" + deck.toString() +
                ", name='" + name + '\'' +
                '}';
    }

    public abstract Card giveCard();

    public static Card getRandomCard (Vector<Card> v) {
        Random generator = new Random();
        int rnd = generator.nextInt(v.size() - 1);
        return v.get(rnd); // Cast the vector value into a String object
    }
}
