package com.ul.acl.model.game;

import com.ul.acl.model.players.Player;

public abstract class Game {

    protected String name;
    protected Player player;

    public Game(String name, Player player) {
        this.name = name;
        this.player=player;
    }

    public String getName() {
        return name;
    }
    public Player getPlayer() {
        return player;
    }


    @Override
    public String toString() {
        return "game{" +
                "name='" + name + '\'' +
                ", player=" + player.toString() +
                '}';
    }
}
