package com.ul.acl.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class StartScreen implements Initializable {

    @FXML
    private Button start;

    @FXML
    private Button score;

    @FXML
    private Button quit;

    private Scene scene;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void start(ActionEvent event) throws IOException {

        //FXMLLoader fxmlLoader = FXMLLoader.load(getClass().getResource("../resources/fxml/GameScreen.fxml"));
        Parent root1 = FXMLLoader.load(getClass().getResource("/fxml/NameScreen.fxml"));
        //GameScreen control = fxmlLoader.getController();
        //control.ChoixNbJoueurs(Integer.parseInt(nbJoueurs));
        Stage stage = new Stage();
        stage.setScene(new Scene(root1));
        stage.setResizable(false);
        stage.show();
        Stage stageActuel = (Stage) start.getScene().getWindow();
        stageActuel.close();


    }


    public void close(ActionEvent event) throws IOException {
        Stage stageActuel = (Stage) start.getScene().getWindow();
        stageActuel.close();
    }

    public void showScore(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ScoreScreen.fxml"));
        //Parent root1 = fxmlLoader.load(getClass().getResource("/fxml/GameScreen.fxml").openStream());
        Parent root1 = fxmlLoader.load();
        ScoreScreen control = fxmlLoader.getController();
        control.createTable();
        Stage stage = new Stage();
        stage.setScene(new Scene(root1));
        stage.setResizable(false);
        stage.show();
        Stage stageActuel = (Stage) quit.getScene().getWindow();
        stageActuel.close();
    }
}
